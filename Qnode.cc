#include "Qnode.h"

using namespace std;

Qnode::Qnode () {
    locked.store(true);
}

Qnode::Qnode(bool value) {
    locked.store(value);
}

Qnode::~Qnode () {

}
