#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#include "AbortableCLHLock.h"

#ifndef NULL
#define NULL 0
#endif


using namespace std;

const static int THREAD_NUM = 4;
const static int LOOP_NUM = 10000;
const static int WAITING_COUNT = 1000000;

__thread Qnode * myNode;
__thread Qnode * myPred;


AtomicInteger64 atmInt;

void *threadFunc(void *params)
{
    myNode = new Qnode(true);
    myPred = NULL;

    class AbortableCLHLock lock = AbortableCLHLock(myNode, myPred); 
    for( int i = 0; i < LOOP_NUM; ++i ) {
        lock.lock();
        for( int j = 0; j < WAITING_COUNT; ++j );
        atmInt++;
        cout << pthread_self() << " : " << atmInt.load() << endl;
        lock.unlock();
    }
    delete myNode;
    return NULL;
}

int main()
{
    pthread_t threads[THREAD_NUM];
    int i;
    atmInt = AtomicInteger64(0);

    for( i = 0; i < THREAD_NUM; ++i ) {
        if(pthread_create(&threads[i], NULL, threadFunc, NULL) != NULL) {
            cout << "[ERROR]: Thread " << i << " during creating thread " << endl;
            exit(0);
        }
        cout << "Thread " << i << " Created" << endl;
    }

    for( i = 0; i < THREAD_NUM; ++i ) {
        if(pthread_join(threads[i], NULL) != NULL) {
            cout << "[ERROR]: Thread " << i << " during thread join" << endl;
            exit(0);
        }
        cout << "Thread " << i << " Joined" << endl;
    }

    return 0;
}
