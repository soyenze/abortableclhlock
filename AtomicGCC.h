/* ================================================================== *
 * Atomic functions using GCC builtins
 * ================================================================== */

#ifndef ATOMICGCC_H
#define ATOMICGCC_H

//#if HAVE_GCC_ATOMICS
#include <stdint.h>

/** -------------------------------------------------------------------------
    Atomic operations using GCC builtins.

    The 'post' version of each operation applies the operation after fetching
    the value.
 */
class AtomicGCC {
public:

  static int32_t add(int32_t theAmount, volatile int32_t *theValue) {
    return __sync_add_and_fetch(theValue, theAmount);
  }

  static int64_t add(int64_t theAmount, volatile int64_t *theValue) {
    return __sync_add_and_fetch(theValue, theAmount);
  }

  static int32_t post_add(int32_t theAmount, volatile int32_t *theValue) {
    return __sync_fetch_and_add(theValue, theAmount);
  }

  static int64_t post_add(int64_t theAmount, volatile int64_t *theValue) {
    return __sync_fetch_and_add(theValue, theAmount);
  }

  static int32_t sub(int32_t theAmount, volatile int32_t *theValue) {
    return __sync_sub_and_fetch(theValue, theAmount);
  }

  static int64_t sub(int64_t theAmount, volatile int64_t *theValue) {
    return __sync_sub_and_fetch(theValue, theAmount);
  }

  static int32_t post_sub(int32_t theAmount, volatile int32_t *theValue) {
    return __sync_fetch_and_sub(theValue, theAmount);
  }

  static int64_t post_sub(int64_t theAmount, volatile int64_t *theValue) {
    return __sync_fetch_and_sub(theValue, theAmount);
  }

  static int32_t inc(volatile int32_t *theValue) {
    return __sync_add_and_fetch(theValue, 1);
  }

  static int64_t inc(volatile int64_t *theValue) {
    return __sync_add_and_fetch(theValue, 1);
  }

  static int32_t post_inc(volatile int32_t *theValue) {
    return __sync_fetch_and_add(theValue, 1);
  }

  static int64_t post_inc(volatile int64_t *theValue) {
    return __sync_fetch_and_add(theValue, 1);
  }

  static int32_t dec(volatile int32_t *theValue) {
    return __sync_sub_and_fetch(theValue, 1);
  }

  static int64_t dec(volatile int64_t *theValue) {
    return __sync_sub_and_fetch(theValue, 1);
  }

  static int32_t post_dec(volatile int32_t *theValue) {
    return __sync_fetch_and_sub(theValue, 1);
  }

  static int64_t post_dec(volatile int64_t *theValue) {
    return __sync_fetch_and_sub(theValue, 1);
  }

  static bool cas(int32_t oldVal, int32_t newVal, volatile int32_t *theValue) {
    return __sync_bool_compare_and_swap(theValue, oldVal, newVal);
  }

  static bool cas(int64_t oldVal, int64_t newVal, volatile int64_t *theValue) {
    return __sync_bool_compare_and_swap(theValue, oldVal, newVal);
  }

  static bool cas(void * oldVal, void * newVal, void * volatile *theValue) {
    return __sync_bool_compare_and_swap(theValue, oldVal, newVal);
  }

  static void memoryBarrier() {
    __sync_synchronize();
  }
};

typedef AtomicGCC AtomicDefault;

//#endif // HAVE_GCC_ATOMICS

#endif // ATOMICGCC_H
