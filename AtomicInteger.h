/* ================================================================== *
 * Atomic integer types.
 * ================================================================== */

#ifndef ATOMICINT_H
#define ATOMICINT_H

//#include <stdint.h>
#include "AtomicGCC.h"

/** -------------------------------------------------------------------------
    Atomic integer class.
 */
template<class T, class Atomics = AtomicDefault>
class AtomicInteger {
public:
  typedef T value_type;

  AtomicInteger() : value_(0) {}
  AtomicInteger(value_type value) : value_(value) {}
  AtomicInteger(const AtomicInteger & value) : value_(value.value_) {}

  value_type loadNoBarrier() const {
    return value_;
  }

  // load-and-acquire
  value_type load() const {
    value_type result = value_;
    Atomics::memoryBarrier();
    return result;
  }

  // store-and-release
  void store(value_type newVal) {
    Atomics::memoryBarrier();
    value_ = newVal;
  }

  // Cast operator
  operator value_type() const {
    return load();
  }

  // Assignment
  AtomicInteger & operator=(value_type newVal) {
    store(newVal);
    return *this;
  }

  AtomicInteger & operator=(const AtomicInteger & newVal) {
    store(newVal.value_);
    return *this;
  }

  // Pre-increment
  value_type operator++() {
    return Atomics::inc(&value_);
  }

  // Post-increment
  value_type operator++(int) {
    return Atomics::post_inc(&value_);
  }

  // Pre-decrement
  value_type operator--() {
    return Atomics::dec(&value_);
  }

  // Post-decrement
  value_type operator--(int) {
    return Atomics::post_dec(&value_);
  }

  // Augmented addition
  value_type operator+=(value_type n) {
    return Atomics::add(n, &value_);
  }

  // Augmented subtraction
  value_type operator-=(value_type n) {
    return Atomics::sub(n, &value_);
  }

  // Get And Set 
  value_type getAndSet(value_type val) {
      value_type result = load();
      store(val);
      return result;
  }

  // Compare and swap
  bool cas(value_type oldVal, value_type newVal) {
    return Atomics::cas(oldVal, newVal, &value_);
  }


private:
  volatile value_type value_;
};

typedef class AtomicInteger<int32_t> AtomicInteger32;
typedef class AtomicInteger<int64_t> AtomicInteger64;
typedef class AtomicInteger<bool> AtomicBoolean;


#endif // ATOMICINT_H
