CC=cc
CPP=g++
CFLAGS=
CPPFLAGS= -g -Wall
LDFLAGS= -lm
INCLUDES= -I ./include/ -I ./
OBJS= AbortableCLHLock.o Qnode.o testCLHLock.o
LIBS= -L ./lib/ -L ./ -lpthread


all: testCLHLock

testCLHLock: $(OBJS)
	$(CPP) -o testCLHLock $(OBJS) $(LIBS)

testCLHLock.o: 
	$(CPP) $(CPPFLAGS) $(INCLUDES) -c testCLHLock.cc

AbortableCLHLock.o: 
	$(CPP) $(CPPFLAGS) $(INCLUDES) -c AbortableCLHLock.cc

Qnode.o: 
	$(CPP) $(CPPFLAGS) $(INCLUDES) -c Qnode.cc

clean:
	rm -rf *.o testCLHLock tags

tags:
	ctags -R ./
