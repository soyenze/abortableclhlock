#include "AtomicInteger.h"

class Qnode {

private:

public:
  AtomicBoolean locked;
  Qnode();
  Qnode(bool value);
  virtual ~Qnode();
};
