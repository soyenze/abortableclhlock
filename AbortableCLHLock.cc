#include "AbortableCLHLock.h"
#include <iostream>

using namespace std;

// myNode & myPred should be initialised before calling this constructor
AbortableCLHLock::AbortableCLHLock(Qnode *myNode, Qnode *myPred): _myNode(myNode), _myPred(myPred) {
    if(tail.load() == NULL)
        tail.store(new Qnode(false));
}

AbortableCLHLock::~AbortableCLHLock() {
}

void AbortableCLHLock::lock() {
    Qnode * pred = tail.getAndSet(_myNode);
    _myPred = pred;

    if( pred != NULL ) {
        while(pred->locked.load()) {}
    }else {
        cout << "pred is NULL" << endl;
    }
}

void AbortableCLHLock::unlock() {
    _myNode->locked.store(false);
    _myNode = _myPred;
}
