/* ================================================================== *
 * Atomic pointer type.
 * ================================================================== */

#ifndef ATOMICPTR_H
#define ATOMICPTR_H

#ifndef NULL
#define NULL 0
#endif

#include "AtomicGCC.h"


/** -------------------------------------------------------------------------
    Atomic pointer.
 */
template<class T, class Atomics = AtomicDefault>
class AtomicPtr {
public:
  typedef T value_type;

  AtomicPtr() : value_(NULL) {}
  AtomicPtr(value_type * value) : value_(value) {}

  // load-and-acquire
  value_type * load() const {
    value_type * result = value_;
    Atomics::memoryBarrier();
    return result;
  }

  // store-and-release
  void store(value_type * newVal) {
    Atomics::memoryBarrier();
    value_ = newVal;
  }

  // Cast operator
  operator value_type *() const {
    return load();
  }

  // Assignment
  AtomicPtr & operator=(value_type * newVal) {
    store(newVal);
    return *this;
  }

  AtomicPtr & operator=(const AtomicPtr & newVal) {
    store(newVal.value_);
    return *this;
  }

  value_type * getAndSet(value_type * val) {
      value_type * result = load();
      store(val);
      return result;
  }

  // Compare and swap
  bool cas(value_type * oldVal, value_type * newVal) {
    return Atomics::cas(oldVal, newVal,
        reinterpret_cast<void * volatile *>(&value_));
  }

private:
  value_type * volatile value_;
};

#endif // ATOMICPTR_H
