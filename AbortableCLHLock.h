#include "AtomicPtr.h"
#include "Lock.h"
#include "Qnode.h"

static AtomicPtr<Qnode> tail;

class AbortableCLHLock : public Lock {

private:
  Qnode * _myNode;
  Qnode * _myPred;

public:
  AbortableCLHLock(Qnode *, Qnode *);
  ~AbortableCLHLock();

  void lock();
  void unlock();
  
};
